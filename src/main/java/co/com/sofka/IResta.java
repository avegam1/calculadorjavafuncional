package co.com.sofka;

public interface IResta {
    Integer calcularResta (Integer numero1, Integer numero2);
}
