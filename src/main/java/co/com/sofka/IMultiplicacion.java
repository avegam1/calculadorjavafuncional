package co.com.sofka;

public interface IMultiplicacion {
    Integer calcularMultiplicacion (Integer numero1, Integer numero2);
}
