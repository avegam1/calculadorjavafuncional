package co.com.sofka;

public interface IDivision {
    Integer calcularDivision (Integer numero1, Integer numero2);
}
