package co.com.sofka;

import java.util.stream.IntStream;

public class Calculadora {
    static ISuma addition = (numero1,numero2) -> numero1+numero2;

    static IResta subtraction = (numero1, numero2) -> numero1-numero2;

    static IMultiplicacion multiplication = (numero1,numero2) -> IntStream.iterate(numero2 < 0 && numero1 < 0 ? -Math.min(numero1,numero2) : Math.min(numero1,numero2), i -> i)
            .limit(Math.abs(Math.max(numero1,numero2)))
            .reduce((cummulative, num) -> addition.calcularsuma(cummulative,num))
            .getAsInt();

    static IDivision division = (numero1,numero2) -> IntStream.iterate(numero1, i -> i-numero2)
            .limit(2)
            .reduce((cummulative, num) -> subtraction.calcularResta(cummulative,num))
            .getAsInt();


    public static void main(String[] args) {
        // Executing
        System.out.println("suma: " + addition.calcularsuma(5,2));

        System.out.println("Resta: " + subtraction.calcularResta(5,2));

        System.out.println("Multiplication: " + multiplication.calcularMultiplicacion(5,-7));

        System.out.println("Division: " + division.calcularDivision(10,2));
    }
}
